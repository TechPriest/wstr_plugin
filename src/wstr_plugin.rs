#![crate_type="dylib"]
#![feature(plugin_registrar, rustc_private)]

extern crate syntax;
extern crate rustc;
extern crate rustc_plugin;
extern crate encoding;

use syntax::parse::token;
use syntax::tokenstream::TokenTree;
use syntax::ext::base::{ExtCtxt, MacResult, DummyResult, MacEager};
use syntax::ext::build::AstBuilder;  // A trait for expr_usize.
use syntax::ext::quote::rt::Span;
use syntax::ptr::P;
use syntax::ast::{Expr, ExprKind, LitKind, LitIntType, UintTy};
use rustc_plugin::Registry;


fn expand_wstr(cx: &mut ExtCtxt, sp: Span, args: &[TokenTree])
        -> Box<MacResult + 'static> {
    use syntax::codemap::dummy_spanned;
    use syntax::ast::DUMMY_NODE_ID;
    use encoding::{Encoding, EncoderTrap};
    use encoding::all::UTF_16LE;

    if args.len() != 1 {
        cx.span_err(
            sp,
            &format!("argument should be a single string, but got {} arguments", args.len()));
        return DummyResult::any(sp);
    }

    let text = match args[0] {
        TokenTree::Token(_, token::Literal(token::Lit::Str_(s), _)) => s.to_string(),
        _ => {
            cx.span_err(sp, "argument should be a single string");
            return DummyResult::any(sp);            
        }
    };

    let text = { 
        let mut text = UTF_16LE.encode(&text, EncoderTrap::Ignore).unwrap();
        assert!(text.len() % 2 == 0);
        text.extend([0u8, 0u8].iter().cloned());
        text
    };

    let len = text.len() / 2;
    
    let mut buf: Vec<P<Expr>> = Vec::new();
    for i in 0 .. len {
        let ndx = i * 2;
        let l = text[ndx];
        let h = text[ndx + 1];
        let c: u16 = (h as u16) << 8 | l as u16;

        let int_lit = LitKind::Int(c as u128, LitIntType::Unsigned(UintTy::U16));
        let span = dummy_spanned(int_lit);

        let e: Expr = Expr {
            id: DUMMY_NODE_ID,
            attrs: syntax::util::ThinVec::new(),
            node: ExprKind::Lit(P(span)),
            span: sp,
        };
        buf.push(P(e));
    }

    MacEager::expr(cx.expr_vec(sp, buf))
}

#[plugin_registrar]
pub fn plugin_registrar(reg: &mut Registry) {
    reg.register_macro("wstr", expand_wstr);
}