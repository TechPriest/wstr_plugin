#![feature(plugin)]
#![plugin(wstr_plugin)]

extern crate winapi;
extern crate kernel32;
extern crate user32;


fn main() {
    use std::ptr::null_mut;
    
    unsafe
    {
        user32::MessageBoxW(null_mut(), 
            wstr!("こんにちは").as_ptr(), 
            wstr!("Привет").as_ptr(), 
            0);
    }
}


